package analysis.heap;

import analysis.graph.Vertex;

import java.util.ArrayList;
import java.util.Collections;

// Doop Var along with its points-to set

public class VariableNode extends Vertex {

    ArrayList<InstanceNode> pointsTo = new ArrayList<InstanceNode>();

    public VariableNode(String id) {
        super(id);
    }

    public String getConcreteTypes() {
        String a = "";
        for(InstanceNode n: pointsTo) {
            a = n.getType()+",";
        }
        return a;
    }

    public void add(InstanceNode a) {
        pointsTo.add(a);
    }

    public InstanceNode sample() {
        Collections.shuffle(pointsTo);
        return pointsTo.get(0).copy();

    }
}
