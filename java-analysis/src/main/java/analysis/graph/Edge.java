package analysis.graph;

import java.util.Objects;

public class Edge {

    private Vertex start;

    public void setStart(Vertex start) {
        this.start = start;
    }

    public void setEnd(Vertex end) {
        this.end = end;
    }

    private Vertex end;
    private String label;

    public Edge(Vertex start, Vertex end, String label) {

        Objects.requireNonNull(start, "Edge must have two vertices");
        Objects.requireNonNull(end, "Edge must have two vertices");
        Objects.requireNonNull(label, "Edge must have a label");

        this.start = start;
        this.end = end;
        this.label = label;
    }

    public Vertex getStart() {
        return start;
    }
    public Vertex getEnd() {
        return end;
    }



    public String getLabel() {
        return label;
    }

}
