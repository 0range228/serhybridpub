package analysis.graph;

import java.util.Objects;

public class Vertex {

    private String id;

    public String toString() {
        return id;
    }
    public String getId() {
        return id;
    }

    public boolean equals(Object o) {
        if (!(o instanceof Vertex))
            return false;
        Vertex other = (Vertex) o;
        return other.id.equals(this.id);
    }

    public int hashCode() {
        return id.hashCode();
    }



    public Vertex(String id) {
        Objects.requireNonNull(id, "Vertex must have an identifier");
        this.id = id;
    }

}
