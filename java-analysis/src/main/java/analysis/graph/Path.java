package analysis.graph;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class Path {

    ArrayList<Edge> edges;
    HashSet<Vertex> vertices;

    public Path() {
        edges = new ArrayList<Edge>();
        vertices = new HashSet<Vertex>();
    }

    // add edge to end of path
    public void addEdge(Edge e) throws IllegalArgumentException {
        if (edges.size() == 0) {
            edges.add(e);
            return;
        }
        Edge lastEdge = edges.get(edges.size()-1);
        if (!lastEdge.getEnd().equals(e.getStart()))
            throw new IllegalArgumentException();
        edges.add(e);

    }


    public List<Edge> getEdges() {
        return edges;
    }

    public String toString() {
        String result = "";

        for(Edge e: edges) {
            Vertex start = e.getStart();
            Vertex end = e.getEnd();
            String label = e.getLabel();
            if (result.equals("")) {
                result = result + start + " -> " + label + " -> " + end;
            } else {
                result = result + " -> " + label + " -> " + end;
            }
        }
        return result;
    }

    // parse a delimiter separated string to a path. every other field in the string is an edge
    public Path(String pathStr, String seperator, boolean reverse) {

        Vertex previous = null;
        String label = null;

        edges = new ArrayList<Edge>();
        vertices = new HashSet<Vertex>();

        String[] components = pathStr.split(seperator);

        if (reverse)
            Collections.reverse(java.util.Arrays.asList(components));

        for(int i = 0; i < components.length; i++) {
            String component = components[i].trim();
            if (i % 2 == 0) {
                Vertex vertex = new Vertex(component);
                vertices.add(vertex);
                if (label == null) {
                    previous = vertex;
                    continue;
                }
                Edge e = new Edge(previous, vertex, label);
                addEdge(e);
                previous = vertex;

            } else {
                label = component;
            }

        }
    }

}
