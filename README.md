# A Hybrid Analysis to Detect Java Serialisation Vulnerabilities

##  Directory structure for the project

`./java-analysis`: Code for analysing output from doop. 

`./instrumenter`: Instrumentation code

`./scripts`: Scripts for running the analysis (doop and randoop)

`./data`: Dataset, `./data/pom.xml` contains versions of the libraries.



