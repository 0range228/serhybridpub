package instrumenter.logger;

import instrumenter.checkers.Checker;

import java.util.ArrayList;
import java.util.List;

public class SinkLogger {

    public static void log(String s) {
        if (Checker.checking)
        System.out.println("sink-reached\t" + s);
    }
}
