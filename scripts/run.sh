#!/usr/bin/env bash


if [ -f "$TARGET_DIR/$1.jar" ]
then
    echo "Found: $TARGET_DIR/$1.jar"
else
    echo "$TARGET_DIR/$1.jar not found. Aborting!" >&2
    exit
fi

CD=`pwd`

mkdir $OUT_DIR/$1


./scripts/run-doop.sh $1 $2
cd $CD
