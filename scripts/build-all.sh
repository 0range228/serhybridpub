#!/usr/bin/env bash

cd instrumenter
mvn clean install dependency:copy-dependencies
cd ..
cd java-analysis
mvn clean install dependency:copy-dependencies
cd ..

