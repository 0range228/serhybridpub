#!/usr/bin/env bash

# creates an instrumented version of input jar file
# adds logging class to jar as well
cd instrumenter
rm input/*
rm -r output/*
cp ../data/$1 input
java -cp target/instrumenter-1.0.0.jar:target/dependencies/* instrumenter.Transform --in input  --out output --log log.log --sinks sinks.txt
mkdir -p output/instrumenter/logger
mkdir -p output/instrumenter/checkers
mkdir -p output/instrumenter/sinks
mkdir -p output/instrumenter/trampolines
cp target/classes/instrumenter/logger/* output/instrumenter/logger
cp target/classes/instrumenter/checkers/* output/instrumenter/checkers
cp target/classes/instrumenter/sinks/* output/instrumenter/sinks
cp target/classes/instrumenter/trampolines/* output/instrumenter/trampolines
cd output
jar cvf $(basename $1 .jar)-instrumented.jar *
cd ../..
