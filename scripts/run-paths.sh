#!/usr/bin/env bash
cd java-analysis
JAR_NAME=$1
echo $JAR_NAME
cp ../classes.txt ../out/$JAR_NAME
cp ../strings.txt ../out/$JAR_NAME
java -Xmx4G -cp target/analysisr-1.0.0.jar:target/dependencies/* analysis.main.Main  --out ../out/$JAR_NAME --log log.log  --t heapinstance
cd -
