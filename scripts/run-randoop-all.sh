#!/bin/bash 

for file in data/*.jar; do
  f=$(basename $file)
  rm instrumenter/input/*
  rm instrumenter/output/*
  eval "./scripts/instrument-jar.sh $f"
  cp instrumenter/log.log out/$JAR_NAME
  eval "./scripts/run-randoop.sh $f"
  exit
done
