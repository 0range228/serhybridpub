#!/bin/bash
#DEBUG=-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=5005
JAR_NAME=$(basename $1 .jar)
if [ -d "out/$JAR_NAME" ] 
then
    rm out/$JAR_NAME/*
else
    mkdir out/$JAR_NAME
fi
rm instrumenter/input/*
rm instrumenter/output/*
eval "./scripts/instrument-jar.sh $1"
cp instrumenter/log.log out/$JAR_NAME
TIME_LIMIT=1800
INPUT_JAR=../instrumenter/output/$JAR_NAME-instrumented.jar
cd scripts
java $DEBUG -cp ../randoop/build/libs/randoop-all-4.2.2.jar:$INPUT_JAR randoop.main.Main gentests  --testjar $INPUT_JAR  --capture-output=true --omit-classes-file=omitted-classes.txt --no-regression-tests --log=randoop-log.txt --usethreads=true --time-limit=$TIME_LIMIT --clear=15
cd -
#mv scripts/Err* out/$JAR_NAME
